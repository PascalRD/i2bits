# i2bits #

## Overview ##
Utility to convert hex or dec to binary format.

## Example ##
hex -> bin
```
$ i2bits 0xfe
11111110
$ i2bits 0xbeaf
10111110.10101111
```
dec -> bin
```
$ i2bits 4242
00010000.10010010
$ i2bits 4294967295
11111111.11111111.11111111.11111111
$ i2bits 18446744073709551614
11111111.11111111.11111111.11111111.11111111.11111111.11111111.11111110
```
bin -> hex
```
$ i2bits -h 11111110
0xfe
```
bin -> dec
```
$ i2bits -d 1000010010010
4242
```

## Install ##

```
$ git clone https://bitbucket.org/PascalRD/i2bits
$ cd i2bits && mkdir build && cd build
$ cmake -DCMAKE_INSTALL_PREFIX=/usr ../
$ make
$ su
$ make install
```

On Gentoo Linux i2bits is available in portage: app-misc/i2bits
